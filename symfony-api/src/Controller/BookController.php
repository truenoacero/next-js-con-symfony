<?php

namespace App\Controller;

use App\Entity\Books;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
    /**
     * @Route ("/api/test",name="test_api")
     */
    public function testResponse(): Response
    {

        $response = new Response();
        $response->setContent(json_encode(array(
            '0' => array(
                'id' => '0',
                'nombre' => 'DDD in PHP',
            ),
                '1' => array(
                    'id' => '1',
                    'nombre' => 'Learning React',
            )
        )));
        return $response;
    }
    /**
     * @Route ("/api/books",name="books")
     */
    public function findAll(): Response
    {
        $books = $this->getDoctrine()->getRepository(Books::class)->findAll();
        $serializer = $this->get('serializer');
        $response = new Response();
        $response->setContent($serializer->serialize($books,'json'));
        return $response;
    }
    /**
     * @Route ("/api/books/{id}",name="books_id")
     */
    public function findById(Request $request): Response
    {
        $id = $request->attributes->get('id');
        $book = $this-> getDoctrine()->getRepository(Books::class)->find($id);
        $response = new Response();
        $serializer = $this->get('serializer');
        $response->setContent($serializer->serialize($book,'json'));
        return $response;
    }
}