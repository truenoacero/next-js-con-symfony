<?php


namespace App\Tests;


use App\Controller\BookController;

class GetBooksTest extends \PHPUnit\Framework\TestCase
{
    public function testBookTestMethod()
    {
        $expectedResult = json_encode(array(
            '0' => array(
                'id' => '0',
                'nombre' => 'DDD in PHP',
            ),
            '1' => array(
                'id' => '1',
                'nombre' => 'Learning React',
            )
        ));
        $bookController = new BookController();
        $resultTestMethod = $bookController->testResponse();
        $this->assertTrue($resultTestMethod->getContent() == $expectedResult);
    }

}