/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/books/[id]";
exports.ids = ["pages/books/[id]"];
exports.modules = {

/***/ "./pages/books/[id].js":
/*!*****************************!*\
  !*** ./pages/books/[id].js ***!
  \*****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"getStaticPaths\": function() { return /* binding */ getStaticPaths; },\n/* harmony export */   \"getStaticProps\": function() { return /* binding */ getStaticProps; }\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n\nvar _jsxFileName = \"/usr/src/pages/books/[id].js\";\nconst getStaticPaths = async () => {\n  const res = await fetch('http://php:8000/api/books');\n  const data = await res.json();\n  const paths = data.map(book => {\n    return {\n      params: {\n        id: book.id.toString()\n      }\n    };\n  });\n  return {\n    paths,\n    fallback: false\n  };\n};\nconst getStaticProps = async context => {\n  const id = context.params.id;\n  const res = await fetch('http://php:8000/api/books/' + id);\n  const data = await res.json();\n  return {\n    props: {\n      book: data\n    }\n  };\n};\n\nconst Book = ({\n  book\n}) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n    children: book.title\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 24,\n    columnNumber: 9\n  }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h3\", {\n    children: \"La descripci\\xF3n del libro\"\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 25,\n    columnNumber: 9\n  }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n    children: book.description\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 26,\n    columnNumber: 9\n  }, undefined)]\n}, void 0, true, {\n  fileName: _jsxFileName,\n  lineNumber: 23,\n  columnNumber: 5\n}, undefined);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Book);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9uZXh0LWFwcC8uL3BhZ2VzL2Jvb2tzL1tpZF0uanM/MTgxZiJdLCJuYW1lcyI6WyJnZXRTdGF0aWNQYXRocyIsInJlcyIsImZldGNoIiwiZGF0YSIsImpzb24iLCJwYXRocyIsIm1hcCIsImJvb2siLCJwYXJhbXMiLCJpZCIsInRvU3RyaW5nIiwiZmFsbGJhY2siLCJnZXRTdGF0aWNQcm9wcyIsImNvbnRleHQiLCJwcm9wcyIsIkJvb2siLCJ0aXRsZSIsImRlc2NyaXB0aW9uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBTyxNQUFNQSxjQUFjLEdBQUcsWUFBVztBQUNyQyxRQUFNQyxHQUFHLEdBQUcsTUFBTUMsS0FBSyxDQUFDLDJCQUFELENBQXZCO0FBQ0EsUUFBTUMsSUFBSSxHQUFHLE1BQU1GLEdBQUcsQ0FBQ0csSUFBSixFQUFuQjtBQUNBLFFBQU1DLEtBQUssR0FBR0YsSUFBSSxDQUFDRyxHQUFMLENBQVNDLElBQUksSUFBSTtBQUM1QixXQUFPO0FBQ0hDLFlBQU0sRUFBRTtBQUFDQyxVQUFFLEVBQUVGLElBQUksQ0FBQ0UsRUFBTCxDQUFRQyxRQUFSO0FBQUw7QUFETCxLQUFQO0FBR0YsR0FKYSxDQUFkO0FBS0EsU0FBTztBQUNITCxTQURHO0FBRUhNLFlBQVEsRUFBRTtBQUZQLEdBQVA7QUFJSCxDQVpNO0FBYUEsTUFBTUMsY0FBYyxHQUFHLE1BQU1DLE9BQU4sSUFBa0I7QUFDNUMsUUFBTUosRUFBRSxHQUFHSSxPQUFPLENBQUNMLE1BQVIsQ0FBZUMsRUFBMUI7QUFDQSxRQUFNUixHQUFHLEdBQUcsTUFBTUMsS0FBSyxDQUFDLCtCQUE2Qk8sRUFBOUIsQ0FBdkI7QUFDQSxRQUFNTixJQUFJLEdBQUcsTUFBTUYsR0FBRyxDQUFDRyxJQUFKLEVBQW5CO0FBQ0EsU0FBTztBQUNIVSxTQUFLLEVBQUU7QUFBQ1AsVUFBSSxFQUFFSjtBQUFQO0FBREosR0FBUDtBQUdILENBUE07O0FBUVAsTUFBTVksSUFBSSxHQUFHLENBQUU7QUFBQ1I7QUFBRCxDQUFGLGtCQUNUO0FBQUEsMEJBQ0k7QUFBQSxjQUFLQSxJQUFJLENBQUNTO0FBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURKLGVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFGSixlQUdJO0FBQUEsY0FBSVQsSUFBSSxDQUFDVTtBQUFUO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFESjs7QUFPQSwrREFBZUYsSUFBZiIsImZpbGUiOiIuL3BhZ2VzL2Jvb2tzL1tpZF0uanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgZ2V0U3RhdGljUGF0aHMgPSBhc3luYygpID0+IHtcbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCgnaHR0cDovL3BocDo4MDAwL2FwaS9ib29rcycpO1xuICAgIGNvbnN0IGRhdGEgPSBhd2FpdCByZXMuanNvbigpO1xuICAgIGNvbnN0IHBhdGhzID0gZGF0YS5tYXAoYm9vayA9PiB7XG4gICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgcGFyYW1zOiB7aWQ6IGJvb2suaWQudG9TdHJpbmcoKSB9XG4gICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiB7XG4gICAgICAgIHBhdGhzLFxuICAgICAgICBmYWxsYmFjazogZmFsc2VcbiAgICB9XG59XG5leHBvcnQgY29uc3QgZ2V0U3RhdGljUHJvcHMgPSBhc3luYyhjb250ZXh0KSA9PiB7XG4gICAgY29uc3QgaWQgPSBjb250ZXh0LnBhcmFtcy5pZDtcbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCgnaHR0cDovL3BocDo4MDAwL2FwaS9ib29rcy8nK2lkKTtcbiAgICBjb25zdCBkYXRhID0gYXdhaXQgcmVzLmpzb24oKTtcbiAgICByZXR1cm4ge1xuICAgICAgICBwcm9wczoge2Jvb2s6IGRhdGF9XG4gICAgfVxufVxuY29uc3QgQm9vayA9ICgge2Jvb2t9ICkgPT4gKFxuICAgIDxkaXY+XG4gICAgICAgIDxoMT57Ym9vay50aXRsZX08L2gxPlxuICAgICAgICA8aDM+TGEgZGVzY3JpcGNpw7NuIGRlbCBsaWJybzwvaDM+XG4gICAgICAgIDxwPntib29rLmRlc2NyaXB0aW9ufTwvcD5cbiAgICA8L2Rpdj5cbik7XG5leHBvcnQgZGVmYXVsdCBCb29rOyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/books/[id].js\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/books/[id].js"));
module.exports = __webpack_exports__;

})();