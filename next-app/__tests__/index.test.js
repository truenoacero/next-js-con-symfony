/**
 * @jest-environment jsdom
 */
import { render, screen } from "@testing-library/react";
import App from "../pages/index";

describe("App", () => {
    beforeEach(() => {
        fetch.resetMocks();
    });
   it("renders ", () => {
       const books = [
           {"id" : 1, "title":'Test'}
       ];
       render(<App books={books} />);
       expect(
           screen.getByRole("heading", {name: "Welcome to Next.js!"})
       ).toBeInTheDocument();
   });
});