export const getStaticPaths = async() => {
    const res = await fetch('http://php:8000/api/books');
    const data = await res.json();
    const paths = data.map(book => {
       return {
           params: {id: book.id.toString() }
       }
    });
    return {
        paths,
        fallback: false
    }
}
export const getStaticProps = async(context) => {
    const id = context.params.id;
    const res = await fetch('http://php:8000/api/books/'+id);
    const data = await res.json();
    return {
        props: {book: data}
    }
}
const Book = ( {book} ) => (
    <div>
        <h1>{book.title}</h1>
        <h3>La descripción del libro</h3>
        <p>{book.description}</p>
    </div>
);
export default Book;