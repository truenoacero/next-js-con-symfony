import Link from 'next/link';

export async function getStaticProps()
{
    const request = await fetch('http://php:8000/api/books');
    const data = await request.json();
    return {
        props: {books : data}
    }
}
const Books = ({books}) => (
    <>
        <h1 className="text-xl border border-red-500 my-2.5 p-2">Ejemplo</h1>
        <ul className="list-inside bg-rose-200">
        {books.map(book=> (
            <Link href={'/books/'+book.id}><a><li className="list-decimal">{book.title}</li></a></Link>
        ))}
        </ul>
    </>
);
export default Books;